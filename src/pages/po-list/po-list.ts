import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { HttpServiceProvider } from '../../providers/http-service/http-service';
import * as Encrypt from 'jsencrypt';
import { HttpSoapProvider } from '../../providers/http-soap/http-soap';


@IonicPage()
@Component({
  selector: 'page-po-list',
  templateUrl: 'po-list.html',
})
export class PoListPage implements OnInit {
  userId: any;
  obj: any = [];
  objSearch: any = [];
  tk: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public url: HttpServiceProvider,
    public alertCtrl: AlertController,
    public httpSoap: HttpSoapProvider,
    public toastCtrl: ToastController
  ) {
    this.userId = navParams.get("param");
  }

  ionViewDidLoad() { }

  ionViewDidEnter() { }

  ngOnInit() {
    let that = this;
    if (that.url.loading) {
      that.url.stopLoading();
    }
    this.url.startLoading().present();
    this.url.checkSession(function (err, resp) {
      if (resp) {
        that.tk = localStorage.getItem("gfdsa");
        localStorage.setItem("newtm", resp);
        that.getPOlist();
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
        that.url.stopLoading();
      }
    });
  }

  doRefresh(refresher) {
    this.ngOnInit();
    setTimeout(() => {
      refresher.complete();
    }, 200);
  }

  initializeItems() {
    this.objSearch = this.obj;
  }

  getItems(ev: any) {
    this.initializeItems();
    let that = this;
    const val = ev.target.value;

    if (val && val.trim() != '') {
      that.objSearch = that.obj.filter((item) => {
        return (item.old[0].STL_TRA_PORELEASE[0].por_ponumber.toString().toLowerCase().indexOf(val.toLowerCase()) > -1 || item.old[0].STL_TRA_PORELEASE[0].por_vendor_name.toString().toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      console.log("search====", that.objSearch);
    }
  }

  getPOlist() {
    var method: string = 'GetListOfPOsPending';
    var parameters: {}[] = [];
    let that = this;
    var encr = new Encrypt.JSEncrypt();
    var encrUser = encr.getAddition1(localStorage.getItem("userId"));
    that.userId = '';
    that.userId = encrUser;
    that.getPOsubCode(parameters, method, that);
  }

  private static userPOcount(username, tk): {}[] {
    var parameters: {}[] = [];

    parameters["userId"] = username;
    parameters["tk"] = tk;

    return parameters;
  }

  envelopeBuilder(requestBody: string): string {
    return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
      "<SOAP:Body>" +
      requestBody +
      "</SOAP:Body>" +
      "</SOAP:Envelope>";
  }

  poDetails(data) {
    this.navCtrl.push('PoDetailsPage', {
      param: data
    })
  }

  getPOsubCode(parameters, method, that) {
    parameters['GetListOfPOsPending xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PoListPage.userPOcount(that.userId, that.tk);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    // that.url.startLoading().present();
    this.httpSoap.testService(parameters, function (err, response) {

      if (response) {
        that.url.stopLoading();
        var temp = response;
        var s = temp['SOAP:Envelope'];
        try {
          var a = s['SOAP:Body'][0].GetListOfPOsPendingResponse;
          var b = a[0].tuple;
          if (b == undefined) {
            let alert = that.alertCtrl.create({
              message: "No data found..!",
              buttons: [{
                text: 'OK',
                handler: () => {
                  that.navCtrl.setRoot("HomePage");
                }
              }]
            });
            alert.present();
          } else {
            that.obj = b;
            that.objSearch = b;
            var formatter = new Intl.NumberFormat('en-US', {
              minimumFractionDigits: 2,
            });

            for (var i = 0; i < that.objSearch.length; i++) {
              that.objSearch[i].old[0].STL_TRA_PORELEASE[0].por_netvalue = formatter.format(that.objSearch[i].old[0].STL_TRA_PORELEASE[0].por_netvalue); /* $2,500.00 */
            }
          }
        }
        catch (e) {
          var a = s['SOAP:Body'][0]['SOAP:Fault'];
          var b = a[0].faultstring[0]._;
          let toast = that.toastCtrl.create({
            message: "Token expired!! Please refresh once..",
            duration: 2500,
            position: "top"
          });
          toast.present();
        }
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
        that.url.stopLoading();
      }
    })
  }
}
