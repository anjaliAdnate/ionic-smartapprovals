import { Component } from "@angular/core";
import { NavParams, ViewController, Platform, App, AlertController, ToastController, NavController } from "ionic-angular";
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { DocumentViewerOptions, DocumentViewer } from "@ionic-native/document-viewer";
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { HttpServiceProvider } from "../../../../providers/http-service/http-service";
import * as Encrypt from 'jsencrypt';
import { HttpSoapProvider } from "../../../../providers/http-soap/http-soap";

@Component({
  template: `
  <ion-list class="maindiv">
    <ion-item *ngFor="let doc of udata" (click)="getDocsListContent(doc)" style="background-color: transparent;">
      <p ion-text text-wrap  style="background-color: transparent; color:white;">{{doc.FILENAME[0]}}</p>
    </ion-item>
  </ion-list>

    `,
  styles: [`

    .maindiv{
      background-color: #0065b3;
    }
    `]
})

export class PopoverPO {
  udata: any[] = [];
  tk: any;
  POnumber: any;
  b64Data: any;
  pdfSrc: any;

  constructor(
    public httpSer: HttpServiceProvider,
    private photoViewer: PhotoViewer,
    private document: DocumentViewer,
    public alertCtrl: AlertController,
    public app: App,
    public platform: Platform,
    public navP: NavParams,
    private file: File,
    private fileOpener: FileOpener,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController,
    public httpSoap: HttpSoapProvider,
    public navCtrl: NavController) {

    console.log("navParams=> ", navP.get("poData"));
    var dData = navP.get("poData");
    this.udata = dData[0].item;
    console.log("udata data: => ", this.udata);
    this.POnumber = navP.get("poNumber");
    this.platform.registerBackButtonAction(() => {
      // Catches the active view
      let nav = this.app.getActiveNavs()[0];
      let activeView = nav.getActive();
      if (activeView.name === "HomePage") {
        const alert = this.alertCtrl.create({
          title: 'App termination',
          message: 'Do you want to close the app?',
          buttons: [{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Application exit prevented!');
            }
          }, {
            text: 'Close App',
            handler: () => {
              this.platform.exitApp(); // Close this application
            }
          }]
        });
        alert.present();
        // }
      } else {
        if (activeView.name === 'PoDetailsPage') {
          this.viewCtrl.dismiss();
          nav.pop();
        } else {
          if (nav.canGoBack()) { //Can we go back?
            nav.pop();
          }
        }
      }

    })
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.viewCtrl.dismiss();
    }, 10000);
  }

  getDocsListContent(doc) {
    let that = this;
    this.viewCtrl.dismiss();
    // that.tk = undefined;
    // this.httpSer.checkSession(function (err, resp) {
    //   if (resp) {
    // that.tk = localStorage.getItem("gfdsa");
    // localStorage.setItem("newtm", resp);
    that.getDocsListContentFunc(doc);
    //   } else {
    //     console.log("not getting response becz of err: ", err)
    //     that.toastCtrl.create({
    //       message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
    //       position: 'bottom',
    //       duration: 3000
    //     }).present();
    //   }
    // });
  }

  getDocsListContentFunc(doc) {
    var parameters: {}[] = [];
    let that = this;
    var encr = new Encrypt.JSEncrypt();

    // var enrPOnumber = encr.getAddition1(that.POnumber[0]);
    // var enrAtchId = encr.getAddition1(doc.old[0].STL_PO_ATTACHMENTS[0].ATTACH_ID[0]);
    // var encryptedPOnum = enrPOnumber;
    // var encryptedAtchID = enrAtchId;

    // var enrPOnumber = encr.getAddition1(that.POnumber[0]);
    // var enrAtchId = encr.getAddition1(doc.DOCID[0]);
    // var encryptedPOnum = enrPOnumber;
    // var encryptedAtchID = enrAtchId;

    var encryptedPOnum = that.POnumber[0];
    var encryptedAtchID = doc.DOCID[0];

    that.getBase64Format(encryptedPOnum, parameters, encryptedAtchID, doc)
  }

  getBase64Format(encryptedPOnum, parameters, enrAtchId, doc) {
    let that = this;

    // parameters['GetPOAttachmentContent xmlns="http://schemas.cordys.com/SterliteSyncModuleDBMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PopoverPO.docsPODetails(encryptedPOnum, enrAtchId, that.tk);
    // parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };

    parameters['GetPOAttachmentContentFromSAP xmlns="http://schemas.cordys.com/SterliteSyncModuleDBMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PopoverPO.docsPODetails(encryptedPOnum, enrAtchId, that.tk);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    that.httpSer.startLoading().present();
    this.httpSoap.testService(parameters, function (err, response) {
      that.httpSer.stopLoading();
      if (response) {
        var temp = response;
        var s = temp['SOAP:Envelope'];
        try {
          // var a = s['SOAP:Body'][0].GetPOAttachmentContentResponse;
          // var b64 = a[0].return;
          // that.b64Data = b64[0];
          // console.log("base64 code: ", that.b64Data)
          // that.download(that.b64Data, doc)

          var a = s['SOAP:Body'][0].GetPOAttachmentContentFromSAPResponse;
          var b64 = a[0].ZMM_PO_READ_FILE_ATTACHMENTResponse;
          that.b64Data = b64[0].EV_FILE[0]._;
          console.log("base64 code: ", that.b64Data)
          that.download(that.b64Data, doc)
        }
        catch (e) {
          var a = s['SOAP:Body'][0]['SOAP:Fault'];
          var b = a[0].faultstring[0]._;
          console.log("token err: ", JSON.stringify(b));
          let toast = that.toastCtrl.create({
            message: "Token expired!! Please refresh once..",
            duration: 2500,
            position: 'top'
          });
          toast.present();
        }
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();

      }
    })
  }

  private static docsPODetails(po_num, atch_id, tk): {}[] {
    var parameters: {}[] = [];
    parameters["poNumber"] = po_num;
    parameters["attachId"] = atch_id;
    // parameters["tk"] = tk;
    return parameters;
  }

  envelopeBuilder(requestBody: string): string {
    return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
      "<SOAP:Body>" +
      requestBody +
      "</SOAP:Body>" +
      "</SOAP:Envelope>";
  }

  getMIMEtype(extn) {
    let ext = extn.toLowerCase();
    let MIMETypes = {
      'msg': 'text/plain',
      'txt': 'text/plain',
      'docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'doc': 'application/msword',
      'pdf': 'application/pdf',
      'jpg': 'image/jpeg',
      'bmp': 'image/bmp',
      'png': 'image/png',
      'xls': 'application/vnd.ms-excel',
      'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'rtf': 'application/rtf',
      'ppt': 'application/vnd.ms-powerpoint',
      'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
      'zip': 'application/zip'
    }
    return MIMETypes[ext];
  }

  download(b64, doc) {
    let downloadPDF: any = b64;
    fetch('data:application/pdf;base64,' + downloadPDF,
      {
        method: "GET"
      }).then(res => res.blob()).then(blob => {
        let fileExtn = doc.FILENAME[0].split('.').reverse()[0];
        console.log("file extention: ", fileExtn)
        let fileMIMEType = this.getMIMEtype(fileExtn);
        if (this.platform.is('android')) {
          // this.file.writeFile(this.file.externalApplicationStorageDirectory, doc.FILENAME[0], blob, { replace: true }).then(res => {
          this.file.writeFile(this.file.cacheDirectory, doc.FILENAME[0], blob, { replace: true }).then(res => {

            var docSrc1 = res.toInternalURL();
            docSrc1 = decodeURIComponent(docSrc1);
            console.log("doc string: ", docSrc1)
            // window.open(docSrc1, '_blank');
            this.fileOpener.open(
              docSrc1,
              fileMIMEType
            ).then((res) => {
              console.log("file opend: ", res)
            }).catch(err => {
              console.log('open error', err)
            });
          }).catch(err => {
            console.log('save error')
          });
        } else if (this.platform.is('ios')) {
          // this.file.writeFile(this.file.documentsDirectory, doc.FILENAME[0], blob, { replace: true }).then(res => {
          this.file.writeFile(this.file.cacheDirectory, doc.FILENAME[0], blob, { replace: true }).then(res => {

            if (fileMIMEType == 'application/pdf') {
              const options: DocumentViewerOptions = {
                title: doc.FILENAME[0]
              }
              this.document.viewDocument(res.nativeURL, fileMIMEType, options);
            } else {
              if (fileMIMEType == 'image/jpeg' || fileMIMEType == 'image/png' || fileMIMEType == 'image/jpg') {
                console.log("nativeURL: ", res.nativeURL)
                var imgSrc = res.nativeURL;
                imgSrc = decodeURIComponent(imgSrc);
                console.log("decodeURIComponent: ", imgSrc)
                this.photoViewer.show(imgSrc);
              } else {
                var docSrc = res.toInternalURL();
                docSrc = decodeURIComponent(docSrc);
                console.log("doc src: ", docSrc)
                this.fileOpener.open(
                  docSrc,
                  fileMIMEType
                ).then((res) => {
                  console.log("file opend: ", res)
                }).catch(err => {
                  console.log('open error', err)
                });
              }
            }
          }).catch(err => {
            console.log('save error')
          });
        }
      }).catch(err => {
        console.log('error')
      });
  }

  // download(b64, doc) {
  //   let downloadPDF: any = b64;
  //   fetch('data:application/pdf;base64,' + downloadPDF,
  //     {
  //       method: "GET"
  //     }).then(res => res.blob()).then(blob => {
  //       let fileExtn = doc.old[0].STL_PO_ATTACHMENTS[0].FILENAME[0].split('.').reverse()[0];
  //       console.log("file extention: ", fileExtn)
  //       let fileMIMEType = this.getMIMEtype(fileExtn);
  //       if (this.platform.is('android')) {
  //         // this.file.writeFile(this.file.externalApplicationStorageDirectory, doc.FILENAME[0], blob, { replace: true }).then(res => {
  //         this.file.writeFile(this.file.cacheDirectory, doc.old[0].STL_PO_ATTACHMENTS[0].FILENAME[0], blob, { replace: true }).then(res => {
  //           this.viewCtrl.dismiss();
  //           var docSrc1 = res.toInternalURL();
  //           docSrc1 = decodeURIComponent(docSrc1);
  //           this.fileOpener.open(
  //             docSrc1,
  //             fileMIMEType
  //           ).then((res) => {
  //             console.log("file opend: ", res)
  //           }).catch(err => {
  //             console.log('open error', err)
  //           });
  //         }).catch(err => {
  //           console.log('save error')
  //         });
  //       } else if (this.platform.is('ios')) {
  //         // this.file.writeFile(this.file.documentsDirectory, doc.FILENAME[0], blob, { replace: true }).then(res => {
  //         this.file.writeFile(this.file.cacheDirectory, doc.old[0].STL_PO_ATTACHMENTS[0].FILENAME[0], blob, { replace: true }).then(res => {
  //           this.viewCtrl.dismiss();
  //           if (fileMIMEType == 'application/pdf') {
  //             const options: DocumentViewerOptions = {
  //               title: doc.old[0].STL_PO_ATTACHMENTS[0].FILENAME[0]
  //             }
  //             this.document.viewDocument(res.nativeURL, fileMIMEType, options);
  //           } else {
  //             if (fileMIMEType == 'image/jpeg' || fileMIMEType == 'image/png' || fileMIMEType == 'image/jpg') {
  //               console.log("nativeURL: ", res.nativeURL)
  //               var imgSrc = res.nativeURL;
  //               imgSrc = decodeURIComponent(imgSrc);
  //               console.log("decodeURIComponent: ", imgSrc)
  //               this.photoViewer.show(imgSrc);
  //             } else {
  //               var docSrc = res.toInternalURL();
  //               docSrc = decodeURIComponent(docSrc);
  //               console.log("doc src: ", docSrc)
  //               this.fileOpener.open(
  //                 docSrc,
  //                 fileMIMEType
  //               ).then((res) => {
  //                 console.log("file opend: ", res)
  //               }).catch(err => {
  //                 console.log('open error', err)
  //               });
  //             }
  //           }
  //         }).catch(err => {
  //           console.log('save error')
  //         });
  //       }
  //     }).catch(err => {
  //       console.log('error')
  //     });
  // }
}

// @Component({
//   template: `
// <ion-content>
// <pdf-viewer *ngIf="pdfSrc != undefined" [src]="pdfSrc"
// [render-text]="true"
// style="display: block;"
// ></pdf-viewer></ion-content>`
// })

// export class DocViewPage {
//   pdfSrc: any;
//   constructor(navParams: NavParams) {
//     console.log("new page nav params: "+ navParams.get("param"));
//     this.pdfSrc = navParams.get("param");
//   }
// }
