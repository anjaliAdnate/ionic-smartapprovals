import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PoDetailsPage } from './po-details';

@NgModule({
  declarations: [
    PoDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PoDetailsPage),
  ],
})
export class PoDetailsPageModule { }
