import { Component } from "@angular/core";
import { NavParams } from "ionic-angular";

@Component({
    template: `
    <table>
        <tr>
            <td>RELEASE CODE:</td>
            <td>{{udata.old[0].PurchaseRequisitionRelease[0].PRR_RELEASECODE}}</td>
        </tr>
            <tr>
            <td>RELEASE GROUP:</td>
            <td>{{udata.old[0].PurchaseRequisitionRelease[0].PRR_RELEASEGROUP}}</td>
        </tr>
        <tr>
            <td>Line Number:</td>
            <td>{{udata.old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER}}</td>
        </tr>
        <tr>
            <td>Material Desc: </td>
            <td>{{udata.old[0].PurchaseRequisitionRelease[0].PRR_MATERIAL_DESC}}</td>
        </tr>
        <tr>
            <td>Qunatity: </td>
            <td>{{udata.old[0].PurchaseRequisitionRelease[0].PRR_QUANTITY}}</td>
        </tr>
        <tr>
            <td>Net Value: </td>
            <td>{{udata.old[0].PurchaseRequisitionRelease[0].PRR_NET_VALUE}}</td>
        </tr>
        <tr>
            <td>GL Account:</td>
            <td>{{udata.old[0].PurchaseRequisitionRelease[0].GL_ACCOUNT}}</td>
        </tr>
    </table>
    `,
    styles: [`
     table {   
        table-layout: fixed;
        width: 100%;
        border-collapse: collapse;
        border: 1px solid black;
       }
      
      thead th:nth-child(1) {
        width: 30%;
      }
      
      thead th:nth-child(2) {
        width: 20%;
      }
      
      thead th:nth-child(3) {
        width: 15%;
      }
      
      thead th:nth-child(4) {
        width: 35%;
      }
      
      th, td {
        padding: 8px;
      }
    `]
})
export class PopoverPR {
    udata: any = [];
    constructor(public navP: NavParams) {
        console.log("navParams=> ", navP.get("prData"));
        this.udata = navP.get("prData");
    }
}