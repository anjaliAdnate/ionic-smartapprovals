import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ToastController, AlertController } from 'ionic-angular';
import { PopoverPR } from './popover-pr/popover-pr';
import { HttpServiceProvider } from '../../../providers/http-service/http-service';
import * as Encrypt from 'jsencrypt';
import { HttpSoapProvider } from '../../../providers/http-soap/http-soap';

@IonicPage()
@Component({
  selector: 'page-pr-details',
  templateUrl: 'pr-details.html',
})
export class PrDetailsPage {
  poData: any;
  obj: any = [];
  reject: boolean;
  approve: boolean;
  comment: string;
  typeAR: any;
  prnum: any;
  lineApp: string = '';
  // values: Array<string> = [];
  pdata: any;
  tempVar: string;
  lineno1: any;
  count: any;
  submitBtnShow: boolean = false;
  activerejectButton: any;
  rejectButton: string;
  rejectButt: boolean;

  tempRejButton: any;
  tempAppButton: any;
  tk: string;
  constructor(
    public httpSer: HttpServiceProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    private toastCtrl: ToastController,
    public url: HttpServiceProvider,
    public alertCtrl: AlertController,
    public httpSoap: HttpSoapProvider
  ) {
    var dTa = navParams.get('param');
    this.count = dTa.old[0].PurchaseRequisitionRelease[0].LINECOUNT[0];
    this.poData = dTa.old[0].PurchaseRequisitionRelease[0];
    this.prnum = this.poData.PRR_PRNUMBER[0];
  }

  ionViewDidLoad() { }

  ionViewDidEnter() { }

  ngOnInit() {
    let that = this;
    this.url.startLoading().present();
    this.url.checkSession(function (err, resp) {
      if (resp) {
        that.tk = localStorage.getItem("gfdsa");
        localStorage.setItem("newtm", resp)
        that.getPRlistDetails();
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
        that.url.stopLoading();
      }
    });
  }

  getPRlistDetails() {
    var method: string = 'GetDetailsOfPR';
    var parameters: {}[] = [];
    let that = this;
    var encr = new Encrypt.JSEncrypt();
    var encrPR = encr.getAddition1(that.poData.PRR_PRNUMBER);
    var updatedPRnum = encrPR;
    that.getPRlistDetailssubCode(parameters, method, that, updatedPRnum);
    // that.url.forExtraSecurity(function (err, resp) {
    //   if (resp) {
    //     localStorage.setItem("newtm", resp)
    //     var encr = new Encrypt.JSEncrypt();
    //     var encrPR = encr.getAddition1(that.poData.PRR_PRNUMBER);
    //     var updatedPRnum = encrPR;
    //     that.getPRlistDetailssubCode(parameters, method, that, updatedPRnum);
    //   } else {
    //     console.log("not getting response becz of err: ", err)
    //     that.toastCtrl.create({
    //       message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
    //       position: 'bottom',
    //       duration: 3000
    //     }).present();
    //     that.url.stopLoading();
    //   }
    // });
  }

  private static userPRcount(pr_num, rel_code, rel_group, tk): {}[] {
    var parameters: {}[] = [];

    parameters["prNumber"] = pr_num;
    parameters["relCode"] = rel_code;
    parameters["relGroup"] = rel_group;
    parameters["tk"] = tk;

    return parameters;
  }

  envelopeBuilder(requestBody: string): string {
    return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
      "<SOAP:Body>" +
      requestBody +
      "</SOAP:Body>" +
      "</SOAP:Envelope>";
  }

  showPopoverPR(ev, data) {
    let popover = this.popoverCtrl.create(PopoverPR,
      {
        prData: data
      },
      {
        cssClass: 'contact-popover'
      });

    popover.onDidDismiss(() => { })

    popover.present({
      ev: ev
    });
  }

  rejectPR() {
    debugger;
    let that = this;
    var parameters: {}[] = [];
    this.url.checkSession(function (err, resp) {
      if (resp) {
        that.tk = "";
        that.tk = localStorage.getItem("gfdsa");
        localStorage.setItem("newtm", resp);
        var encr = new Encrypt.JSEncrypt();
        var encrPR = encr.getAddition1(that.poData.PRR_PRNUMBER);
        var updatedPRnum = encrPR;
        that.rejectPRFunc(updatedPRnum, that.tk, parameters);
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
        that.url.stopLoading();
      }
    });

  }

  rejectPRFunc(updatedPRnum, tkToken, parameters) {
    var method: string = 'UpdatePRApprovals';

    // var parameters: any;
    parameters['UpdatePRApprovals xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PrDetailsPage.userPRApprove(updatedPRnum, this.poData.PRR_RELEASECODE, this.poData.PRR_RELEASEGROUP, this.typeAR, '', '', 'R', tkToken);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    let that = this;
    this.url.startLoading().present();
    this.httpSoap.testService(parameters, function (err, response) {
      if (response) {
        that.url.stopLoading();
        var temp = response;
        var s = temp['SOAP:Envelope'];
        var a = s['SOAP:Body'][0].UpdatePRApprovalsResponse;
        var b = a[0].return;
        if (b == "true") {
          let toast = that.toastCtrl.create({
            message: 'Your request is processed!',
            duration: 2000,
            position: 'bottom'
          });

          toast.onDidDismiss(() => {
            that.navCtrl.setRoot('HomePage');
          });

          toast.present();
        } else {
          let toast = that.toastCtrl.create({
            message: 'Something went wrong!',
            duration: 2000,
            position: 'bottom'
          });

          toast.onDidDismiss(() => {
          });

          toast.present();
        }

      }
    })
  }

  approvePR() {
    debugger;
    let that = this;
    var parameters: {}[] = [];
    this.url.checkSession(function (err, resp) {
      if (resp) {
        that.tk = "";
        that.tk = localStorage.getItem("gfdsa");
        localStorage.setItem("newtm", resp);
        var encr = new Encrypt.JSEncrypt();
        var encrPR = encr.getAddition1(that.poData.PRR_PRNUMBER);
        var updatedPRnum = encrPR;
        that.approvePRFunc(updatedPRnum, that.tk, parameters);
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
        that.url.stopLoading();
      }
    });
  }

  approvePRFunc(updatedPRnum, tkToken, parameters) {
    var method: string = 'UpdatePRApprovals';
    // var parameters: any;
    parameters['UpdatePRApprovals xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PrDetailsPage.userPRApprove(updatedPRnum, this.poData.PRR_RELEASECODE, this.poData.PRR_RELEASEGROUP, this.typeAR, '', '', 'A', tkToken);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };

    let that = this;
    this.url.startLoading().present();
    this.httpSoap.testService(parameters, function (err, response) {
      that.url.stopLoading();
      if (response) {
        var temp = response;
        var s = temp['SOAP:Envelope'];
        var a = s['SOAP:Body'][0].UpdatePRApprovalsResponse;
        var b = a[0].return;
        if (b == "true") {
          let toast = that.toastCtrl.create({
            message: 'Your request is processed!',
            duration: 2000,
            position: 'bottom'
          });

          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
            that.navCtrl.setRoot('HomePage');
          });

          toast.present();
        } else {
          let toast = that.toastCtrl.create({
            message: 'Something went wrong!',
            duration: 2000,
            position: 'bottom'
          });

          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
            // that.navCtrl.setRoot('HomePage');
          });

          toast.present();
        }

      }
    })
  }

  private static userPRApprove(pr_num, rel_code, rel_group, typeApprove, lineApprove, headapcomm, headAprrove, tk): {}[] {
    debugger
    var parameters: {}[] = [];
    parameters["prNumber"] = pr_num;
    parameters["relCode"] = rel_code;
    parameters["relGroup"] = rel_group;
    parameters["typeOfApproval"] = typeApprove;
    parameters["lineApprovals"] = lineApprove;
    parameters["headerApprovalComments"] = headapcomm;
    parameters["headerApproval"] = headAprrove;
    parameters["tk"] = tk;

    return parameters;
  }

  approveFunc(p) {
    p.tax = "approve";
    p.approvebuttonColor = '#32db64';
    p.rejectbuttonColor = '#8f8f91';
    p.comment = undefined;
    this.radioChecked(p);
  }

  rejectFunc(p) {
    p.tax = "reject";
    p.approvebuttonColor = '#8f8f91';
    p.rejectbuttonColor = '#f53d3d';
    this.radioChecked(p);
  }

  radioChecked(p) {
    if (p.tax == "approve") {
      if (p.showBTN == true) {
        p.showBTN = false;
      }
      let that = this;

      var lineno = p.old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER[0];
      that.lineApp = that.lineApp + lineno + "@@@A@@@NOCOMMENTS###";
      var pattern = new RegExp(lineno + "@@@R@@@[a-zA-Z0-9]*###");
      that.lineApp = that.lineApp.replace(pattern, "");
      this.comment = undefined;
      p.values = undefined;
    } else {
      if (p.tax == "reject") {

        p.showBTN = true;
        let that = this;
        var lineno = p.old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER[0];
        that.lineApp = that.lineApp.replace(lineno + "@@@A@@@NOCOMMENTS###", "");
        that.lineApp = that.lineApp + lineno + "@@@R@@@NOCOMMENTS###";
        this.comment = undefined;
      }
    }
  }

  addComment(comm, data, i) {
    data.values = comm;
    let that = this;
    var lineno = data.old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER[0];
    that.lineApp = that.lineApp.replace(lineno + "@@@R@@@NOCOMMENTS###", "");
    that.lineApp = that.lineApp + lineno + "@@@R@@@" + data.comment + "###";
    data.showBTN = false;
    data.comment = undefined;
  }

  lineApprove(data) {
    this.tempVar = "SET";
    let that = this;
    if (data.approve) {
      var lineno = data.old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER[0];
      that.lineApp = that.lineApp + lineno + "@@@A@@@NOCOMMENTS###";
    } else {
      var lineno = data.old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER[0];
      that.lineApp = that.lineApp.replace(lineno + "@@@A@@@NOCOMMENTS###", "");
    }
  }

  lineReject(data) {
    this.tempVar = "SET";
    let that = this;
    var lineno = data.old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER[0];
    that.lineApp = that.lineApp + lineno + "@@@R@@@" + that.comment + "###";
  }

  submitApproval() {
    var method: string = 'UpdatePRApprovals';
    var parameters: {}[] = [];
    let that = this;
    this.url.startLoading().present();
    this.url.checkSession(function (err, resp) {
      if (resp) {
        that.tk = "";
        that.tk = localStorage.getItem("gfdsa");
        localStorage.setItem("newtm", resp);
        var encr = new Encrypt.JSEncrypt();
        var encrPR = encr.getAddition1(that.poData.PRR_PRNUMBER);
        var updatedPRnum = encrPR;
        that.submitAppsubCode(parameters, method, that, updatedPRnum, that.tk);
        // that.url.forExtraSecurity(function (err, resp) {
        //   if (resp) {
        //     localStorage.setItem("newtm", resp)
        //     var encr = new Encrypt.JSEncrypt();
        //     var encrPR = encr.getAddition1(that.poData.PRR_PRNUMBER);
        //     var updatedPRnum = encrPR;
        //     that.submitAppsubCode(parameters, method, that, updatedPRnum, that.tk);
        //   } else {
        //     console.log("not getting response becz of err: ", err)
        //     that.toastCtrl.create({
        //       message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
        //       position: 'bottom',
        //       duration: 3000
        //     }).present();
        //     that.url.stopLoading();
        //   }
        // });
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
        that.url.stopLoading();
      }
    });
  }

  getPRlistDetailssubCode(parameters, method, that, updatedPRnum) {
    parameters['GetDetailsOfPR xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PrDetailsPage.userPRcount(updatedPRnum, that.poData.PRR_RELEASECODE, that.poData.PRR_RELEASEGROUP, that.tk);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };

    // this.url.startLoading().present();
    this.httpSoap.testService(parameters, function (err, response) {
      if (response) {
        that.url.stopLoading();
        var temp = response;
        var s = temp['SOAP:Envelope'];
        if (s['SOAP:Body'] == undefined) {
          let alert = that.alertCtrl.create({
            message: 'Something went wrong. Please visit after some time.',
            buttons: [{
              text: 'Okay'
            }]
          });
          alert.present();
        } else {
          try {
            var a = s['SOAP:Body'][0].GetDetailsOfPRResponse;
            var b = a[0].tuple;
            debugger
            for (var i = 0; i < b.length; i++) {
              b.approve = false;
              b.reject = false;
              b.tax = [];
              b.approvebuttonColor = "#8f8f91";
              b.rejectbuttonColor = "#8f8f91";
              b.values = [];
              b.showBTN = false;
              b.comment = undefined;
            }

            that.obj = b;
            var formatter = new Intl.NumberFormat('en-US', {
              minimumFractionDigits: 2,
            });
            for (var i = 0; i < that.obj.length; i++) {
              var str = that.obj[i].old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER.toString();
              var r = str.replace(/^0+|0+$/, "");
              that.obj[i].old[0].PurchaseRequisitionRelease[0].LineItemsNew = r;
              that.obj[i].old[0].PurchaseRequisitionRelease[0].isoDate = new Date(that.obj[i].old[0].PurchaseRequisitionRelease[0].PRRLINE_CREATED_DATE).toISOString();
              that.obj[i].old[0].PurchaseRequisitionRelease[0].fracValue = formatter.format(that.obj[i].old[0].PurchaseRequisitionRelease[0].PRR_NET_VALUE); /* $2,500.00 */
            }
            that.typeAR = that.obj[0].old[0].PurchaseRequisitionRelease[0].PR_REL_TYPE[0];
            if (that.obj[0].old[0].PurchaseRequisitionRelease[0].PR_REL_TYPE[0] == 'h') {

              that.typeAR = that.obj[0].old[0].PurchaseRequisitionRelease[0].PR_REL_TYPE[0];
            } else {
              that.typeAR = 'l';
            }
          }
          catch (e) {
            var a = s['SOAP:Body'][0]['SOAP:Fault'];
            var b = a[0].faultstring[0]._;
            let toast = that.toastCtrl.create({
              message: "Token expired!! Please refresh once..",
              duration: 2500,
              position: top
            });
            toast.present();
          }
        }
      } else {
        that.url.stopLoading();
      }
    })
  }

  submitAppsubCode(parameters, method, that, updatedPRnum, tkToken) {
    parameters['UpdatePRApprovals xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PrDetailsPage.userPRApprove(updatedPRnum, that.poData.PRR_RELEASECODE, that.poData.PRR_RELEASEGROUP, that.typeAR, that.lineApp, '', '', tkToken);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };

    // this.url.startLoading().present();
    this.httpSoap.testService(parameters, function (err, response) {
      if (response) {
        that.url.stopLoading();
        var temp = response;
        var s = temp['SOAP:Envelope'];
        var a = s['SOAP:Body'][0].UpdatePRApprovalsResponse;
        var b = a[0].return;
        if (b == "true") {
          let toast = that.toastCtrl.create({
            message: 'Your request has been processed sucessfully!',
            duration: 2000,
            position: 'bottom'
          });

          toast.onDidDismiss(() => {
            that.navCtrl.setRoot('HomePage');
          });

          toast.present();
        } else {
          let toast = that.toastCtrl.create({
            message: 'Something went wrong!',
            duration: 2000,
            position: 'bottom'
          });

          toast.onDidDismiss(() => { });

          toast.present();
        }
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
        that.url.stopLoading();
      }
    })
  }

}