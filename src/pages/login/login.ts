import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, ToastController } from 'ionic-angular';
import { IonPullUpFooterState } from 'ionic-pullup';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpServiceProvider } from '../../providers/http-service/http-service';
import * as Encrypt from 'jsencrypt';
import { HttpSoapProvider } from '../../providers/http-soap/http-soap';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',

})
export class LoginPage {

  footerState: IonPullUpFooterState;
  footerState1: IonPullUpFooterState;
  private loginForm: FormGroup;
  private signupForm: FormGroup;
  startText: string = "LOGIN";
  isUnchanged: boolean = false;
  chVal: number = 10;
  choosenNumberValue: number;
  response_number: number;
  footerState2: number;
  validity: string;
  otPpass: string;
  signupuser: any;
  signinObj: any;
  fotterflag: number;
  footerfooter: any;
  otpformactive: any;
  password_length: string;
  isUnchanged1: boolean = false;

  constructor(
    public url: HttpServiceProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public events: Events,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public httpSoap: HttpSoapProvider
  ) {
    this.footerState = IonPullUpFooterState.Collapsed;

    this.loginForm = this.formBuilder.group({
      email: ['', Validators.email],
      password: ['', Validators.required],
    });

    this.signupForm = this.formBuilder.group({
      email: ['', Validators.email]
    });

    this.startText = "LOGIN";

    if (localStorage.getItem("EmailIdEntered") != null) {
      var emailSTr = localStorage.getItem("EmailIdEntered");
      this.loginForm.patchValue({ 'email': emailSTr })
    }
  }

  username: string = '';
  password: string = '';

  ionViewDidLoad() {
    var test: any = localStorage.getItem('footer');
    if (test == 1) {
      this.footerState2 = 0;
    }
  }

  ionViewDidEnter() { }

  footerExpanded() { }

  footerCollapsed() {
    if (this.footerState == 1) {
      this.footerState = 0;
    } else if (this.footerState == 1) {
      this.footerState = 1;
    }
  }

  toggleFooter() {
    this.footerState = this.footerState == IonPullUpFooterState.Collapsed ? IonPullUpFooterState.Expanded : IonPullUpFooterState.Collapsed;
  }

  logForm() {

    if (this.loginForm.valid) {
      this.isUnchanged1 = true;
      var res = this.loginForm.value.email.split("@");
      var userid = res[0];

      var method: string = 'AuthenticateStlUser';
      var parameters: {}[] = [];

      this.username = userid;
      this.password = this.loginForm.value.password;

      let that = this;

      localStorage.setItem("userId", that.username);
      this.url.startLoading().present();
      this.url.forExtraSecurity(function (err, resp) {
        if (resp) {
          localStorage.setItem("newtm", resp)
          var encr = new Encrypt.JSEncrypt();
          that.username = encr.getAddition1(that.username);
          that.password = encr.getAddition2(that.password);
          that.callSubCode(parameters, method);
        } else {
          console.log("not getting response becz of err: ", err)
          that.toastCtrl.create({
            message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
            position: 'bottom',
            duration: 3000
          }).present();
          that.isUnchanged1 = false;
          that.url.stopLoading();
        }
      });
    }
  }

  callSubCode(parameters, method) {
    let that = this;
    parameters['AuthenticateStlUser xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/" xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = LoginPage.userLogin(that.username, that.password);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    // this.url.startLoading().present();
    this.httpSoap.testService(parameters, function (err, response) {
      that.url.stopLoading();
      debugger
      if (response) {
        var s = response['SOAP:Envelope'];
        var a = s['SOAP:Body'][0].AuthenticateStlUserResponse;
        var b = a[0].tuple[0].old[0].authenticateStlUser[0].authenticateStlUser[0];
        console.log("login debugger:", b)
        if (b == "INVALID CREDENTIALS/SAP ID Not Exists") {
          that.toastCtrl.create({
            message: b,
            duration: 2000,
            position: 'bottom'
          }).present();
          that.isUnchanged1 = false;
          // that.navCtrl.setRoot('LoginPage');
        } else {
          var strstr = b.split('#');
          var str1 = strstr[3];
          var str2 = str1.split(':');
          var pocount = str2[1];
          var str3 = strstr[4];
          var str4 = str3.split(':');
          var prcount = str4[1];

          let toast = that.toastCtrl.create({
            message: 'You have logged in successfully',
            duration: 2000,
            position: 'bottom'
          });

          toast.onDidDismiss(() => {
            that.events.publish("user:set", b);
            localStorage.setItem("userName", b);
            localStorage.setItem("oldPass", that.loginForm.value.password)
            localStorage.setItem("LOGGEDIN", "LOGGEDIN");
            localStorage.setItem("EmailIdEntered", that.loginForm.value.email);
            that.navCtrl.setRoot('HomePage', {
              "pocount_param": pocount,
              "prcount_param": prcount
            });
          });
          toast.present();
        }
      } else {
        console.log("error found in err tag: ", err)
        console.log("no response cought")
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
        that.isUnchanged1 = false;
      }
    });
  }

  private static userLogin(username, password): {}[] {
    var parameters: {}[] = [];
    parameters["userId"] = username;
    parameters["password"] = password;
    return parameters;
  }

  envelopeBuilder(requestBody: string): string {
    return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
      "<SOAP:Body>" +
      requestBody +
      "</SOAP:Body>" +
      "</SOAP:Envelope>";
  }

  signup() {
    const signupFormEmail = this.signupForm.get('email');
    const loginFormEmail = this.loginForm.get('email');
    signupFormEmail.setValue(loginFormEmail.value);
    let that = this;
    if (that.footerState1 == 0) {
      that.footerState1 = 1;
    } else {
      if (that.footerState1 == 1) {
        that.footerState = 0;
        that.footerState1 = 0;
      }
    }
  }

  login() {
    let that = this;
    that.footerState1 = 0;
    that.startText = "LOGIN";
    that.footerState = 1;
  }

  login2() {
    let that = this;
    that.footerState2 = 0;
    that.footerState1 = 0;
    that.startText = "LOGIN";
    that.footerState = 0;
  }

  doSignUp() {

    this.signinObj = this.signupForm.value.email;
    localStorage.setItem('userdetail', this.signinObj)
    localStorage.setItem("EmailIdEntered", this.signupForm.value.email);
    if (this.signinObj) {
      this.isUnchanged = true;
      var str = this.signupForm.value.email.split('@');
      var aft = str[1];
      if (aft == 'sterlite.com' || aft == 'adnatesolutions.com' || aft == 'oneqlik.in') {
        var method: string = 'CreateSTLAuthUserOTP';
        var parameters: {}[] = [];
        // debugger;
        this.username = (this.signupForm.value.email).toString().toLowerCase();
        this.password_length = "4";
        this.validity = "30";

        let that = this;

        this.url.forExtraSecurity(function (err, resp) {
          if (resp) {
            console.log("resp from callback to login screen=> ", resp)
            localStorage.setItem("newtm", resp)
            var encr = new Encrypt.JSEncrypt();
            encr.getAddition1(that.username);

            that.username = encr.getAddition1(that.username);
            // that.password = encr.getAddition2(that.password);

            console.log("encrypted signup username: ", that.username);
            console.log("encrypted signup password: ", that.password_length)
            that.signupSubFunc(parameters, method);
          } else {
            console.log("not getting response becz of err: ", err)
            that.toastCtrl.create({
              message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
              position: 'bottom',
              duration: 3000
            }).present();
            // that.isUnchanged1 = false;
            that.isUnchanged = false;
          }
        });

        // that.signupSubFunc(parameters, method);

      } else {
        let alert = this.alertCtrl.create({
          message: 'Please provide valid Sterlite e-mail id.',
          buttons: [{
            text: 'Okay',
            handler: () => {
              this.signupForm.reset();
              this.isUnchanged = false;
            }
          }]
        });
        alert.present();
      }
    }
  }
  private static userSignup(username, password, validity): {}[] {
    var parameters: {}[] = [];
    parameters["EMAIL_ID"] = username;
    parameters["PWD_LENGTH"] = password;
    parameters["VALIDITY"] = validity;
    return parameters;
  }

  signupSubFunc(parameters, method) {
    parameters['CreateSTLAuthUserOTP  xmlns="http://schemas.cordys.com/default"'] = LoginPage.userSignup(this.username, this.password_length, this.validity);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    let that = this;
    this.url.startLoading().present();
    this.httpSoap.testService(parameters, function (err, response) {
      that.url.stopLoading();
      if (response) {
        console.log("otp response: ", response)
        debugger
        var s = response['SOAP:Envelope'];
        var a = s['SOAP:Body'][0].CreateSTLAuthUserOTPResponse;
        var b = a[0].OUTMSG[0]._;
        if(b == 'Your OTP has been sent on your e-mail.') {
          let toast = that.toastCtrl.create({
            message: b,
            duration: 3000,
            position: 'bottom'
          });
  
          toast.onDidDismiss(() => {
            that.signupForm.value.email = "";
            that.navCtrl.setRoot("OtPpagePage", {
              "param": that.username
            });
            that.footerState1 = 1;
          });
          toast.present();
        } else {
          let toast = that.toastCtrl.create({
            message: b,
            duration: 3000,
            position: 'bottom'
          });
  
          toast.onDidDismiss(() => {
            that.signupForm.value.email = "";
            that.navCtrl.setRoot("LoginPage");
            that.footerState1 = 1;
          });
          toast.present();
        }
        
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
        that.isUnchanged = false;
      }
    })
  }
}
