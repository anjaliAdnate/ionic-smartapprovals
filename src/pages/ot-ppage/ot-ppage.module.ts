import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtPpagePage } from './ot-ppage';
// import { SoapService } from '../../providers/soap-service/soap-service';

@NgModule({
  declarations: [
    OtPpagePage,
  ],
  imports: [
    IonicPageModule.forChild(OtPpagePage),
  ],
  providers: [
    //  {provide: String, useValue: "SoapService"}
    // SoapService
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class OtPpagePageModule {}
