import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Pro } from '@ionic/pro';
import { Injectable, Injector } from '@angular/core';
import { MyApp } from './app.component';
import { PopoverPO } from '../pages/po-list/po-details/popover-po/popover-po';
import { HttpServiceProvider } from '../providers/http-service/http-service';
import { HttpModule } from '@angular/http';
import { PopoverPR } from '../pages/pr-list/pr-details/popover-pr/popover-pr';
import { IonicStorageModule } from '@ionic/storage';
import { HttpSoapProvider } from '../providers/http-soap/http-soap';
import { HTTP } from '@ionic-native/http';
import { HttpClientModule } from '@angular/common/http';
import { URLS } from '../providers/urls';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { PhotoViewer } from '@ionic-native/photo-viewer';
// import { PdfViewerModule } from 'ng2-pdf-viewer';

Pro.init('e3a06060', {
  appVersion: '0.0.1'
})

@Injectable()
export class MyErrorHandler implements ErrorHandler {
  ionicErrorHandler: IonicErrorHandler;

  constructor(injector: Injector) {
    try {
      this.ionicErrorHandler = injector.get(IonicErrorHandler);
    } catch (e) {
      // Unable to get the IonicErrorHandler provider, ensure
      // IonicErrorHandler has been added to the providers list below
    }
  }

  handleError(err: any): void {
    Pro.monitoring.handleNewError(err);
    // Remove this if you want to disable Ionic's auto exception handling
    // in development mode.
    this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
  }
}

@NgModule({
  declarations: [
    MyApp,
    PopoverPO,
    PopoverPR,
    // DocViewPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule,
    HttpClientModule,
    // PdfViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PopoverPO,
    PopoverPR,
    // DocViewPage
  ],
  providers: [
    // { provide: String, useValue: "SoapService" },
    StatusBar,
    SplashScreen,
    HTTP,
    IonicErrorHandler,
    [{ provide: ErrorHandler, useClass: MyErrorHandler }],
    HttpServiceProvider,
    HttpSoapProvider,
    URLS,
    File,
    FileOpener,
    DocumentViewer,
    PhotoViewer,
  ],

})
export class AppModule { }
