import { Component } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { App } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(
    public platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public app: App,
    public alertCtrl: AlertController,
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.initializeAPP();

    platform.registerBackButtonAction(() => {
      let nav = app.getActiveNavs()[0];
      let activeView = nav.getActive();
      if (activeView.name === "HomePage") {
        const alert = this.alertCtrl.create({
          title: 'App termination',
          message: 'Do you want to close the app?',
          buttons: [{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Application exit prevented!');
            }
          }, {
            text: 'Close App',
            handler: () => {
              this.platform.exitApp(); // Close this application
            }
          }]
        });
        alert.present();
      } else {
          if (nav.canGoBack()) { //Can we go back?
            nav.pop();
          }
      }
    });

  }

  initializeAPP() {
    if (localStorage.getItem('LOGGEDIN') != null) {
      this.rootPage = "HomePage";
    } else {
      this.rootPage = "LoginPage";
      console.log("Login page")
    }
  }
}

