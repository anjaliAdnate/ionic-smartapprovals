import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as xml2js from "xml2js"
import { HTTP } from '@ionic-native/http';
import { URLS } from '../urls';

@Injectable()
export class HttpSoapProvider {
  headers: any;
  private envelopeBuilder_: (requestBody: string) => string = null;

  constructor(
    public urls: URLS,
    private http: HTTP,
    public httpnew: HttpClient) {
    console.log('Hello HttpSoapProvider Provider');
    this.headers = new HttpHeaders({ 'Content-Type': 'text/xml;charset="utf-8"' })
  }

  private toXml(parameters: any): string {
    var xml: string = "";
    var parameter: any;

    switch (typeof (parameters)) {
      case "string":
        xml += parameters.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        break;

      case "number":
      case "boolean":
        xml += parameters.toString();
        break;

      case "object":
        if (parameters.constructor.toString().indexOf("function Date()") > -1) {
          let year: string = parameters.getFullYear().toString();
          let month: string = ("0" + (parameters.getMonth() + 1).toString()).slice(-2);
          let date: string = ("0" + parameters.getDate().toString()).slice(-2);
          let hours: string = ("0" + parameters.getHours().toString()).slice(-2);
          let minutes: string = ("0" + parameters.getMinutes().toString()).slice(-2);
          let seconds: string = ("0" + parameters.getSeconds().toString()).slice(-2);
          let milliseconds: string = parameters.getMilliseconds().toString();

          let tzOffsetMinutes: number = Math.abs(parameters.getTimezoneOffset());
          let tzOffsetHours: number = 0;

          while (tzOffsetMinutes >= 60) {
            tzOffsetHours++;
            tzOffsetMinutes -= 60;
          }

          let tzMinutes: string = ("0" + tzOffsetMinutes.toString()).slice(-2);
          let tzHours: string = ("0" + tzOffsetHours.toString()).slice(-2);

          let timezone: string = ((parameters.getTimezoneOffset() < 0) ? "-" : "+") + tzHours + ":" + tzMinutes;

          xml += year + "-" + month + "-" + date + "T" + hours + ":" + minutes + ":" + seconds + "." + milliseconds + timezone;
        }
        else if (parameters.constructor.toString().indexOf("function Array()") > -1) { // Array
          for (parameter in parameters) {
            if (parameters.hasOwnProperty(parameter)) {
              if (!isNaN(parameter)) {  // linear array
                (/function\s+(\w*)\s*\(/ig).exec(parameters[parameter].constructor.toString());

                var type = RegExp.$1;

                switch (type) {
                  case "":
                    type = typeof (parameters[parameter]);
                    break;
                  case "String":
                    type = "string";
                    break;
                  case "Number":
                    type = "int";
                    break;
                  case "Boolean":
                    type = "bool";
                    break;
                  case "Date":
                    type = "DateTime";
                    break;
                }
                xml += this.toElement(type, parameters[parameter]);
              }
              else { // associative array
                xml += this.toElement(parameter, parameters[parameter]);
              }
            }
          }
        }
        else { // Object or custom function
          for (parameter in parameters) {
            if (parameters.hasOwnProperty(parameter)) {
              xml += this.toElement(parameter, parameters[parameter]);
            }
          }
        }
        break;

      default:
        throw new Error("SoapService error: type '" + typeof (parameters) + "' is not supported");
    }

    return xml;
  }

  private static stripTagAttributes(tagNamePotentiallyWithAttributes: string): string {
    tagNamePotentiallyWithAttributes = tagNamePotentiallyWithAttributes + ' ';

    return tagNamePotentiallyWithAttributes.slice(0, tagNamePotentiallyWithAttributes.indexOf(' '));
  }

  private toElement(tagNamePotentiallyWithAttributes: string, parameters: any): string {
    var elementContent: string = this.toXml(parameters);

    if ("" == elementContent) {
      return "<" + tagNamePotentiallyWithAttributes + "/>";
    }
    else {
      return "<" + tagNamePotentiallyWithAttributes + ">" + elementContent + "</" + HttpSoapProvider.stripTagAttributes(tagNamePotentiallyWithAttributes) + ">";
    }
  }

  set envelopeBuilder(envelopeBuilder: (response: {}) => string) {
    this.envelopeBuilder_ = envelopeBuilder;
  }

  set envelopeBuilder2(envelopeBuilder2: (response: {}) => string) {
    this.envelopeBuilder_ = envelopeBuilder2;
  }

  testService(input, cb: any) {

    var request: string = this.toXml(input);
    var envelopedRequest: string = null != this.envelopeBuilder_ ? this.envelopeBuilder_(request) : request;

    this.http.setSSLCertMode('pinned').then((res) => {
      
      let headers = {
        'Content-Type': 'text/xml;charset="utf-8"',
        "Accept": "text/xml",
        'origin': 'Mobile'
      };

      let soapurl = this.urls._baseURL;
    
      let xml = envelopedRequest;
      this.http.setDataSerializer('utf8');
      this.http.post(soapurl, xml, headers)
        .then((data) => {
          xml2js.parseString(data.data, function (err, result) {
            if (result == undefined) {
              cb(err, result)
            } else {
              cb(err, result)
            }
          });
        }).catch((err) => {
          xml2js.parseString(err.error, function (err, result) {
            if (result == undefined) {
              cb(err, result)
            } else {
              cb(err, result)
            }
          });
        })
    });
  }
}
